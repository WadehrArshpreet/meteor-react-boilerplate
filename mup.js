module.exports = {
    servers: {
        one: {
            "host": "hosy",
            "username": "ubuntu",
            "pem": "./bff.pem"
            // pem:
            // or leave blank for authenticate from ssh-agent
        }
    },
    meteor: {
        name: 'App',
        path: '.',
        servers: {
            one: {},
        },
        buildOptions: {
            serverOnly: true,
        },
        env: {
            PORT: 3000,
            // MONGO_URL: "mongodb://username:password@host:port/dbname",
            MONGO_URL: "mongodb url",
            ROOT_URL: 'https://imeyou.io/'
        },
        // change to 'kadirahq/meteord' if your app is not using Meteor 1.4
        docker: {
          image: 'abernix/meteord:node-8.4.0-base' // for 1.6
        },
        deployCheckWaitTime: 160,
        // Show progress bar while uploading bundle to server
        // You might need to disable it on CI servers
        enableUploadProgressBar: true
    }
};
