import React from 'react';

export default class Header extends React.Component {
  render() {
    return (
      <header className='Header'>
        <b>Header</b> &nbsp;
        <a href="/">Home</a> &nbsp;
        <a href="about">About Page</a> &nbsp;
        <a href="bad-url">Not Found Page</a> &nbsp;
      </header>
    );
  }
}
