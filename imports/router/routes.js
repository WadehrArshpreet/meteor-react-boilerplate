import {FlowRouter} from 'meteor/kadira:flow-router'
import React from 'react'
import { mount } from 'react-mounter'
import Home from '../ui/pages/Home.jsx';
import About from '../ui/pages/About.jsx';
import NotFound from '../ui/pages/NotFound.jsx';
import MainLayout from '../ui/containers/MainLayout.jsx';


FlowRouter.route('/', {
  name: 'Home',
  action: function () {
    console.log("a");
    mount(MainLayout, {content: <Home />})
  }
});
FlowRouter.route('/about', {
  name: 'About',
  action: function () {
    mount(MainLayout, {content: <About />})
  }
});
FlowRouter.route('/*', {
  name: 'Not Found',
  action: function () {
    mount(MainLayout, {content: <NotFound />})
  }
});
